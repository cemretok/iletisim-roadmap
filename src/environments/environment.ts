// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  getCommunicationInquiry: 'http://localhost:7001/communication-roadmap/api/communication/inquiry',
  addCommunication: 'http://localhost:7001/communication-roadmap/api/communication/add',
  updateCommunication: 'http://localhost:7001/communication-roadmap/api/communication/update',
  deleteCommunication: 'http://localhost:7001/communication-roadmap/api/communication/delete',
  getProductInquiry: 'http://localhost:7001/communication-roadmap/api/product/inquiry',
  getProductInquiryById: 'http://localhost:7001/communication-roadmap/api/product/inquiryById',
  getProductInquiryByCategoryAndType: 'http://localhost:7001/communication-roadmap/api/product/inquiryByCategoryAndType',
  addProduct: 'http://localhost:7001/communication-roadmap/api/product/add',
  addToTimelineProduct: 'http://localhost:7001/communication-roadmap/api/product/addToTimeline',
  updateProduct: 'http://localhost:7001/communication-roadmap/api/product/update',
  deleteProduct: 'http://localhost:7001/communication-roadmap/api/product/delete',
  getTimelineInquiry: 'http://localhost:7001/communication-roadmap/api/timeline/inquiry',
  getTimelineInquiryById: 'http://localhost:7001/communication-roadmap/api/timeline/inquiryById',
  getTimelineInquiryLastOne: 'http://localhost:7001/communication-roadmap/api/timeline/inquiryLastOne',
  getTimelineInquiryActiveList: 'http://localhost:7001/communication-roadmap/api/timeline/inquiryActiveList',
  addTimeline: 'http://localhost:7001/communication-roadmap/api/timeline/add',
  excel: 'http://localhost:7001/communication-roadmap/api/timeline/excel',
  basicAuth: 'http://localhost:7001/communication-roadmap/api/login/basicAuth'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
