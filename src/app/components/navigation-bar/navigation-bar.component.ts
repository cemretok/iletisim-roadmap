import { TimelineService } from './../../services/timeline.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'src/app/services/navbar.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommunicationService } from 'src/app/services/communication.service';
import { ProductService } from 'src/app/services/product.service';
import { DataService } from 'src/app/services/data.service';
import { AppSettings } from 'src/app/AppSettings';
import { TimelineData } from 'src/app/data/TimelineData';
import * as moment from 'moment';
import { Product } from 'src/app/data/Product';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit {
  isPreviewOn = true;
  filter = '';
  navbarData = [];
  category = [];
  selectedCategory = [];
  type = [];
  selectedType = [];
  categoryAndTypeCriteria = [];
  message: string;

  submitted = false;
  categoryRequired = true;
  typeRequired = true;
  isFormInvalid = true;

  constructor(private navbarService: NavbarService,
    public authentocationService: AuthenticationService,
    private router: Router,
    private modalService: NgbModal,
    private communicationService: CommunicationService,
    private productService: ProductService,
    private data: DataService,
    private timelineService: TimelineService) { }

  ngOnInit(): void {
    this.communicationService.getCommunicationInquiry(AppSettings.CATEGORY).subscribe(val => this.category = val.data);
    this.communicationService.getCommunicationInquiry(AppSettings.TYPE).subscribe(val => this.type = val.data);
  }

  searchFilter(input) {
    this.filter = input;
    this.sendNavbarService();
  }

  changePreview() {
    if (this.isPreviewOn === true) {
      this.isPreviewOn = false;
    } else if (this.isPreviewOn === false) {
      this.isPreviewOn = true;
    }

    this.sendNavbarService();
  }

  sendNavbarService() {
    this.navbarData = [];
    this.navbarData.push(this.isPreviewOn);
    this.navbarData.push(this.filter);

    this.navbarService.changeMessage(this.navbarData);
  }

  logout() {
    this.authentocationService.logOut();
    this.router.navigate(['login']);
  }
  openCommunicationPopUp(content) {
    this.modalService.open(content);
  }

  createNewTimeline() {
    const products = JSON.parse(sessionStorage.getItem('products')) as Product[];
    const newTimelineDetails = [];

    products.forEach((k, v) => {
      const tempProduct = { id: null, product: null };
      tempProduct.id = 0;
      tempProduct.product = k;

      newTimelineDetails.push(tempProduct);
    });

    const newTimeline: TimelineData = {
      name: moment().format('YYYY-MM-DD HH:mm:ss'),
      createDate: moment().format('DD-MM-YYYY HH:mm:ss'),
      timelineDetails: newTimelineDetails
    } as TimelineData;

    this.timelineService.addTimeline(newTimeline).subscribe(val => {
      window.location.reload();
    });
  }


  createExcel() {
    const downloadLink = document.createElement('a');

    downloadLink.href = this.timelineService.createExcel(sessionStorage.getItem('timeline'));
    downloadLink.click();
  }

  getTimelineByCriteria() {
    this.submitted = true;
    if (this.selectedCategory === undefined) {
      this.categoryRequired = false;
    } else if (this.selectedCategory !== undefined) {
      this.categoryRequired = true;
    }

    if (this.selectedType === undefined) {
      this.typeRequired = false;
    } else if (this.selectedType !== undefined) {
      this.typeRequired = true;
    }

    this.isFormInvalid = this.categoryRequired && this.typeRequired;

    if (this.isFormInvalid === false) {
      return;
    } else {
      this.categoryAndTypeCriteria = [];

      this.selectedCategory.forEach((k, v) => {
        this.categoryAndTypeCriteria.push(this.category.find(e => e.id === k));
      });

      this.selectedType.forEach((k, v) => {
        this.categoryAndTypeCriteria.push(this.type.find(e => e.id === k));
      });

      this.productService.inquiryByCategoryAndType(this.categoryAndTypeCriteria).subscribe(val => {
        this.data.changeMessage(val);
      });
    }
  }
}