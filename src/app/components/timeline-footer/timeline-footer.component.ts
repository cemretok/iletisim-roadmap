import { ProductDataService } from './../../services/product-data.service';
import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-timeline-footer',
  templateUrl: './timeline-footer.component.html',
  styleUrls: ['./timeline-footer.component.scss']
})
export class TimelineFooterComponent implements OnInit {

  @Input('montlyPrice') set montlyPrice(items: []) {
    this.monthlyPriceList = items;
  }

  priceList = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  monthlyPriceList = [];
  totalPrice = 0;

  constructor(private productDataService: ProductDataService) {
    this.productDataService.currentMessage.subscribe(val => {
      if (val != null) {
        this.splitBudgetMontly(val);
      }
    });
  }

  ngOnInit() {
  }

  getTotalPrice(val: any[]) {
    this.totalPrice = 0;
    for (let v of val) {
      this.totalPrice += +v;
    }
    return this.totalPrice;
  }

  splitBudgetMontly(timeline: any) {
    const year = moment().format('YYYY');

    this.priceList.forEach((pk, pv) => {
      let monthlyBudget = 0;
      const startOfMonth = moment([year, pv]).startOf('month').format('DD-MM-YYYY');
      const endOfMonth = moment([year, pv]).endOf('month').format('DD-MM-YYYY');
      timeline.forEach((tk, tv) => {
        const dailyBudget = tk.budget / this.timeDifferance(tk.startDate, tk.endDate);

        const startMidOfMonth = moment(tk.startDate, 'DD-MM-YYYY') >= moment(startOfMonth, 'DD-MM-YYYY')
          && moment(tk.startDate, 'DD-MM-YYYY') <= moment(endOfMonth, 'DD-MM-YYYY'); // ayın ortasında başlıyorsa
        const endAfterThisMonth = moment(tk.endDate, 'DD-MM-YYYY') > moment(endOfMonth, 'DD-MM-YYYY'); // bir sonraki aylarda bitiyorsa
        const beforeThisMonth = moment(tk.startDate, 'DD-MM-YYYY') < moment(startOfMonth, 'DD-MM-YYYY'); // ayın başından önce başlamışsa
        const afterThisMonth = moment(tk.endDate, 'DD-MM-YYYY') > moment(endOfMonth, 'DD-MM-YYYY'); // ayın sonunda sonra bitmişse
        const startBeforeThisMonth = moment(tk.startDate, 'DD-MM-YYYY') < moment(startOfMonth, 'DD-MM-YYYY'); // ayın başında önce başlamış
        const endMidOfMonth = moment(tk.endDate, 'DD-MM-YYYY') <= moment(endOfMonth, 'DD-MM-YYYY')
          && moment(tk.endDate, 'DD-MM-YYYY') >= moment(startOfMonth, 'DD-MM-YYYY'); // ayın ortasında bitiyorsa

        if (startMidOfMonth && endAfterThisMonth) { // ayın ortasında başlıyorsa ve bir sonraki aya geçiyorsa
          monthlyBudget += this.timeDifferance(tk.startDate, endOfMonth) * dailyBudget;
        } else if (beforeThisMonth && afterThisMonth) {  // tüm ayı kapsıyorsa
          monthlyBudget += this.timeDifferance(startOfMonth, endOfMonth) * dailyBudget;
        } else if (startBeforeThisMonth && endMidOfMonth) { // ayın ortasında bitiyorsa
          monthlyBudget += this.timeDifferance(startOfMonth, tk.endDate) * dailyBudget;
        } else if (startMidOfMonth && endMidOfMonth) { // bir ay içinde bitiyorsa
          monthlyBudget += this.timeDifferance(tk.startDate, tk.endDate) * dailyBudget;
        }

      });
      this.monthlyPriceList[pv] = monthlyBudget;
    });

  }
  timeDifferance(startDate, endDate) {
    return moment(endDate, 'DD-MM-YYYY').diff(moment(startDate, 'DD-MM-YYYY'), 'day') + 1;
  }

}
