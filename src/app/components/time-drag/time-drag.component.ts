import { Router } from '@angular/router';
import { ProductDataService } from './../../services/product-data.service';
import { NavbarService } from './../../services/navbar.service';
import { Product } from './../../data/Product';
import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, Input, ChangeDetectorRef } from '@angular/core';
import {
  CompactType, DisplayGrid, Draggable, GridsterConfig, GridsterItem, GridType, PushDirections, Resizable,
  GridsterItemComponentInterface
} from 'angular-gridster2';
import * as moment from 'moment';
import { Moment } from 'moment';

interface Safe extends GridsterConfig {
  draggable: Draggable;
  resizable: Resizable;
  pushDirections: PushDirections;
}

@Component({
  selector: 'app-time-drag',
  templateUrl: './time-drag.component.html',
  styleUrls: ['./time-drag.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class TimeDragComponent implements OnInit {
  @Input('timeDragMessage') set timeDragMessage(items: []) {
    this.item = items;
    this.dashboard = [];

    this.item.forEach((tk: any, tv: number) => {
      this.addItem(tk.id, tk.color, moment(tk.startDate, 'DD-MM-YYYY'), moment(tk.endDate, 'DD-MM-YYYY'));
      this.category = tk.category.name;
      this.totalBudget += tk.budget;
    });
  }

  isPreviewOn = true;
  options: Safe;
  dashboard: Array<GridsterItem>;
  item: [];
  heightMultiplier = 1;
  height = 60;
  dynamicHeight = 60;
  category;
  totalBudget = 0;
  dashboardItemChange = false;
  products: Product[];
  changedProducts: Product[];

  constructor(private navbarService: NavbarService,
              private changeDetectorRef: ChangeDetectorRef,
              private productDataService: ProductDataService,
              private router: Router) {
  }

  ngOnInit() {
    this.options = {
      //gridType: GridType.HorizontalFixed,
      gridType: GridType.Fit,
      itemChangeCallback: this.itemChange.bind(this),
      compactType: CompactType.None,
      margin: 0,
      outerMargin: true,
      outerMarginTop: null,
      outerMarginRight: null,
      outerMarginBottom: null,
      outerMarginLeft: null,
      useTransformPositioning: true,
      mobileBreakpoint: 640,
      minCols: 365,
      maxCols: 365,
      //minCols: 52,
      //maxCols: 52,
      minRows: 1,
      maxRows: 20,
      maxItemCols: 365,
      minItemCols: 1,
      maxItemRows: 1,
      minItemRows: 1,
      maxItemArea: 2500,
      minItemArea: 1,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedColWidth: 20,
      fixedRowHeight: 20,
      keepFixedHeightInMobile: false,
      keepFixedWidthInMobile: false,
      scrollSensitivity: 10,
      scrollSpeed: 20,
      enableEmptyCellClick: false,
      enableEmptyCellContextMenu: false,
      enableEmptyCellDrop: false,
      enableEmptyCellDrag: false,
      enableOccupiedCellDrop: false,
      emptyCellDragMaxCols: 50,
      emptyCellDragMaxRows: 50,
      ignoreMarginInRow: true,
      draggable: {
        enabled: true,
      },
      resizable: {
        enabled: true,
        handles: { s: false, e: true, n: false, w: true, se: false, ne: false, sw: false, nw: false }
      },
      swap: false,
      pushItems: true,
      disablePushOnDrag: true,
      disablePushOnResize: true,
      pushDirections: { north: true, east: true, south: true, west: true },
      pushResizeItems: false,
      displayGrid: DisplayGrid.None,
      disableWindowResize: false,
      disableWarnings: false,
      scrollToNewItems: false
    };

    this.options['maxRows'] = this.heightMultiplier + 1;
    this.changedOptions();

    //console.log(this.heightMultiplier);

    this.navbarService.currentMessage.subscribe(val => {
      if (val != null) {
        this.isPreviewOn = val[0];
        this.changeDetectorRef.markForCheck();
      }
    });
  }

  changedOptions() {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
  }

  removeItem($event, item) {
    $event.preventDefault();
    $event.stopPropagation();
    this.dashboard.splice(this.dashboard.indexOf(item), 1);
  }

  addItem(itemId: number, itemColor: string, startDate: Moment, endDate: Moment) {
    let row = 0;
    const startDateCoordination = this.dayTimeToCoordinationConverter(startDate);
    const endDateCoordination = this.dayTimeToCoordinationConverter(endDate);
    const dateBetween = endDateCoordination - startDateCoordination;

    this.dashboard.map(val => {
      const firsCoordinations = [];
      const secondCoordinations = [];
      for (let i = startDateCoordination; i <= endDateCoordination; i++) { firsCoordinations.push(i); }
      for (let i = val.x; i <= (val.cols + val.x); i++) { secondCoordinations.push(i); }

      if (firsCoordinations.some(r => secondCoordinations.includes(r))) {
        row += 1;
        if (this.heightMultiplier < row) {
          this.heightMultiplier = row;
        }
      }

      this.dynamicHeight = (this.heightMultiplier + 1) * this.height;
      // console.log("itemColor " + itemColor);
      // console.log("this.dynamicHeight " +  this.dynamicHeight);
      // console.log("this.heightMultiplier " + this.heightMultiplier);
      // console.log("this.height " + this.height);
      //this.options['maxRows'] = this.heightMultiplier;
      //this.changedOptions();

    });

    this.dashboard.push({
      id: itemId,
      color: itemColor,
      x: startDateCoordination,
      y: row,
      cols: dateBetween,
      rows: 1
    });
  }

  dayTimeToCoordinationConverter(date: Moment) {
    return date.diff(moment().startOf('year'), 'day');
  }

  coordinationToDayTimeConverter(x: number, format: string) {
    return moment(moment().dayOfYear(x + 1), format).format(format);
  }

  timeDifferance(startDate: Moment, endDate: Moment) {
    return endDate.diff(startDate, 'day');
  }

  getItemName(id) {
    const value: Product[] = this.item;
    return value.find(x => x.id === id).name;
  }

  itemChange(item: GridsterItem, itemComponent: GridsterItemComponentInterface) {
    this.dashboardItemChange = true;

    this.products = JSON.parse(sessionStorage.getItem('products'));
    const itemIndex = this.products.findIndex(val => val.id === item.id);

    this.products[itemIndex].startDate = this.coordinationToDayTimeConverter(item.x, 'DD-MM-YYYY');
    this.products[itemIndex].endDate = this.coordinationToDayTimeConverter(item.x + item.cols, 'DD-MM-YYYY');

    this.productDataService.changeMessage(this.products);
  }

  deleteItem(id) {
    this.dashboard = this.dashboard.filter(val => val.id !== id);

    this.products = JSON.parse(sessionStorage.getItem('products'));
    this.products = this.products.filter(val => val.id !== id);

    this.productDataService.changeMessage(this.products);
  }

  cloneItem(id) {
    this.router.navigate(['urun/clone/' + id]);
  }

  updateItem(id) {
    this.router.navigate(['urun/update/' + id]);
  }
}
