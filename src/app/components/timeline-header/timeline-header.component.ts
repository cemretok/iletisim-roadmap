import { AppSettings } from '../../AppSettings';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommunicationService } from 'src/app/services/communication.service';
import { ProductService } from 'src/app/services/product.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-timeline-header',
  templateUrl: './timeline-header.component.html',
  styleUrls: ['./timeline-header.component.scss']
})
export class TimelineHeaderComponent implements OnInit {
  category = [];
  selectedCategory = [];
  type = [];
  selectedType = [];
  categoryAndTypeCriteria = [];
  message: string;

  submitted = false;
  categoryRequired = true;
  typeRequired = true;
  isFormInvalid = true;

  monthList = ['OCAK', 'ŞUBAT', 'MART', 'NİSAN', 'MAYIS', 'HAZİRAN', 'TEMMUZ', 'AĞUSTOS', 'EYLÜL', 'EKİM', 'KASIM', 'ARALIK'];
  weekList = ['1', '2', '3', '4', '1', '2', '3', '4', '1', '2', '3', '4',
    '1', '2', '3', '4', '1', '2', '3', '4', '1', '2', '3', '4',
    '1', '2', '3', '4', '1', '2', '3', '4', '1', '2', '3', '4',
    '1', '2', '3', '4', '1', '2', '3', '4', '1', '2', '3', '4'];

  constructor(private modalService: NgbModal,
              private communicationService: CommunicationService,
              private productService: ProductService,
              private data: DataService) { }

  ngOnInit() {

  }

  openCommunicationPopUp(content) {
    this.modalService.open(content);
  }

}
