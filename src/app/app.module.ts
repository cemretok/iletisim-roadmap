import { LoginComponent } from './pages/login/login.component';
import { ReversePipe } from './pipe/reverse.pipe';
import { OrderPipe } from './pipe/order.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './pages/index/index.component';
import { CommunicationTypeComponent } from './pages/communication-type/communication-type.component';
import { CommunicationCategoryComponent } from './pages/communication-category/communication-category.component';
import { ProductComponent } from './pages/product/product.component';
import { NavigationBarComponent } from './components/navigation-bar/navigation-bar.component';
import { CommunicationTypeAddComponent } from './pages/communication-type-add/communication-type-add.component';
import { CommunicationCategoryAddComponent } from './pages/communication-category-add/communication-category-add.component';
import { TimeDragComponent } from './components/time-drag/time-drag.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { GridsterModule } from 'angular-gridster2';
import { TimelineHeaderComponent } from './components/timeline-header/timeline-header.component';
import { TimelineFooterComponent } from './components/timeline-footer/timeline-footer.component';
import { FilterPipe } from './pipe/filter.pipe';
import { CommunicationCategoryDeleteComponent } from './pages/communication-category-delete/communication-category-delete.component';
import { CommunicationTypeDeleteComponent } from './pages/communication-type-delete/communication-type-delete.component';
import localtr from '@angular/common/locales/tr';
registerLocaleData(localtr);
@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    CommunicationTypeComponent,
    CommunicationCategoryComponent,
    ProductComponent,
    NavigationBarComponent,
    CommunicationTypeAddComponent,
    CommunicationCategoryAddComponent,
    TimeDragComponent,
    TimelineHeaderComponent,
    TimelineFooterComponent,
    LoginComponent,
    OrderPipe,
    ReversePipe,
    FilterPipe,
    CommunicationCategoryDeleteComponent,
    CommunicationTypeDeleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, ReactiveFormsModule,
    NgSelectModule,
    NgbModule,
    HttpClientModule,
    DragDropModule,
    GridsterModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'tr-TR' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
