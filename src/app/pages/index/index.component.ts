import { NavbarService } from './../../services/navbar.service';
import { Product } from './../../data/Product';
import { TimelineData } from './../../data/TimelineData';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { TimelineService } from './../../services/timeline.service';
import { DataService } from 'src/app/services/data.service';
import { ProductDataService } from 'src/app/services/product-data.service';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  searchText = '';
  isPreviewOn = true;
  timeline = [];
  timelineGroup = [];
  priceList = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  timelineVersion = [];
  constructor(private timelineService: TimelineService,
    private data: DataService,
    private navbarService: NavbarService,
    private productDataService: ProductDataService) {
    this.data.currentMessage.subscribe(message => {
      if (message !== null) {
        this.bindProductToTimeline(message);
        this.productDataService.changeMessage(this.timeline);
      }
    });

    this.navbarService.currentMessage.subscribe(val => {
      if (val != null) {
        this.isPreviewOn = val[0];
        this.searchText = val[1];
      }
    });
  }

  ngOnInit(): void {
    this.timelineService.getTimelineInquiryLastOne().subscribe(val => {
      this.bindTimelineDetailsToTimeline(val);
      this.productDataService.changeMessage(this.timeline);
      sessionStorage.setItem('timeline', val.data.id.toString());
    });

    this.timelineService.getTimelineInquiryActiveList().subscribe(val => {
      const tempTimelineVersion = [];
      for (const value of Object.values(val.data)) {
        tempTimelineVersion.push(value);
      }
      this.timelineVersion = tempTimelineVersion.slice().reverse();
    });
  }

  getLastTimelineId() {
    return this.timelineVersion[0].id;
  }

  changeTimelineVersion(timelineId: string) {
    this.timelineService.getTimelineInquiryById(timelineId).subscribe(val => {
      this.bindTimelineDetailsToTimeline(val);
      this.productDataService.changeMessage(this.timeline);
      sessionStorage.setItem('timeline', timelineId);
    });
  }

  bindTimelineDetailsToTimeline(val) {
    this.timeline = [];
    if (val.data != null) {
      val.data.timelineDetails.forEach((k, v) => {
        this.timeline.push(k.product);
      });
    }


    this.timelineGroup = Object.values(this.groupByProduct(this.timeline));
    this.splitBudgetMontly(this.timeline);
  }

  bindProductToTimeline(val) {
    this.timeline = [];
    val.data.forEach((k, v) => {
      this.timeline.push(k);
    });

    this.timelineGroup = Object.values(this.groupByProduct(this.timeline));
    this.splitBudgetMontly(this.timeline);
  }

  groupByProduct(timelineDetails: any[]): any {
    return timelineDetails.reduce((r, a) => {
      r[a.category.name] = [...r[a.category.name] || [], a];
      return r;
    }, {});
  }

  splitBudgetMontly(timeline: any) {
    const year = moment().format('YYYY');

    this.priceList.forEach((pk, pv) => {
      let monthlyBudget = 0;
      const startOfMonth = moment([year, pv]).startOf('month').format('DD-MM-YYYY');
      const endOfMonth = moment([year, pv]).endOf('month').format('DD-MM-YYYY');
      this.timeline.forEach((tk, tv) => {
        const dailyBudget = tk.budget / this.timeDifferance(tk.startDate, tk.endDate);

        const startMidOfMonth = moment(tk.startDate, 'DD-MM-YYYY') >= moment(startOfMonth, 'DD-MM-YYYY')
          && moment(tk.startDate, 'DD-MM-YYYY') <= moment(endOfMonth, 'DD-MM-YYYY'); // ayın ortasında başlıyorsa
        const endAfterThisMonth = moment(tk.endDate, 'DD-MM-YYYY') > moment(endOfMonth, 'DD-MM-YYYY'); // bir sonraki aylarda bitiyorsa
        const beforeThisMonth = moment(tk.startDate, 'DD-MM-YYYY') < moment(startOfMonth, 'DD-MM-YYYY'); // ayın başından önce başlamışsa
        const afterThisMonth = moment(tk.endDate, 'DD-MM-YYYY') > moment(endOfMonth, 'DD-MM-YYYY'); // ayın sonunda sonra bitmişse
        const startBeforeThisMonth = moment(tk.startDate, 'DD-MM-YYYY') < moment(startOfMonth, 'DD-MM-YYYY'); // ayın başında önce başlamış
        const endMidOfMonth = moment(tk.endDate, 'DD-MM-YYYY') <= moment(endOfMonth, 'DD-MM-YYYY')
          && moment(tk.endDate, 'DD-MM-YYYY') >= moment(startOfMonth, 'DD-MM-YYYY'); // ayın ortasında bitiyorsa

        if (startMidOfMonth && endAfterThisMonth) { // ayın ortasında başlıyorsa ve bir sonraki aya geçiyorsa
          monthlyBudget += this.timeDifferance(tk.startDate, endOfMonth) * dailyBudget;
        } else if (beforeThisMonth && afterThisMonth) {  // tüm ayı kapsıyorsa
          monthlyBudget += this.timeDifferance(startOfMonth, endOfMonth) * dailyBudget;
        } else if (startBeforeThisMonth && endMidOfMonth) { // ayın ortasında bitiyorsa
          monthlyBudget += this.timeDifferance(startOfMonth, tk.endDate) * dailyBudget;
        } else if (startMidOfMonth && endMidOfMonth) { // bir ay içinde bitiyorsa
          monthlyBudget += this.timeDifferance(tk.startDate, tk.endDate) * dailyBudget;
        }

      });
      this.priceList[pv] = monthlyBudget;
    });

  }

  timeDifferance(startDate, endDate) {
    return moment(endDate, 'DD-MM-YYYY').diff(moment(startDate, 'DD-MM-YYYY'), 'day') + 1;
  }
}
