import { AppSettings } from '../../AppSettings';
import { CommunicationService } from './../../services/communication.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-communication-type',
    templateUrl: './communication-type.component.html',
    styleUrls: ['./communication-type.component.scss']
})
export class CommunicationTypeComponent implements OnInit {

    public data = [];
    type = [];
    selectedType = [];



    constructor(private communicationService: CommunicationService) {
    }

    ngOnInit() {
        this.communicationService.getCommunicationInquiry(AppSettings.TYPE).subscribe(val => this.type = val.data);
    }
}
