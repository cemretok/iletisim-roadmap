import { AppSettings } from './../../AppSettings';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { AuthResponse } from 'src/app/data/AuthResponse';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  invalidLogin = false;
  authResponse;

  constructor(private router: Router, private loginservice: AuthenticationService) { }

  ngOnInit() {
  }

  formChange() {
    this.invalidLogin = false;
  }

  checkLogin() {
    this.authResponse = this.loginservice.authenticate(this.username, this.password).subscribe((val: AuthResponse) => {
      if (val.returnCode === AppSettings.RETURN_CODE_SUCCESS) {
        this.router.navigate(['index']);
        sessionStorage.setItem('username', this.username);
        this.invalidLogin = false;
      } else {
        this.invalidLogin = true;
      }
    });

  }

}
