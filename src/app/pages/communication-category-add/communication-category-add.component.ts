import { AppSettings } from './../../AppSettings';
import { CommunicationResponse } from './../../data/CommunicationResponse';
import { Component, OnInit } from '@angular/core';
import { CommunicationService } from 'src/app/services/communication.service';
import { Communication } from 'src/app/data/Communication';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-communication-category-add',
  templateUrl: './communication-category-add.component.html',
  styleUrls: ['./communication-category-add.component.scss']
})
export class CommunicationCategoryAddComponent implements OnInit {

  communucation = {} as Communication;
  communucationResponse = {} as CommunicationResponse;
  categoryName;
  category;
  added = false;
  cantAdd = false;
  categoryNameLenght = AppSettings.CATEGORY_NAME_LENGHT;
  categoryForm: any;
  submitted = false;

  constructor(private router: Router, private communicationService: CommunicationService) {
  }

  ngOnInit() {
    this.categoryForm = new FormGroup({
      categoryName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(this.categoryNameLenght)])
    });
  }

  get f() { return this.categoryForm.controls; }

  formChange() {
    this.submitted = false;
    this.added = false;
    this.cantAdd = false;
  }

  timeoutTooltip() {
    setTimeout(() => this.added = false, AppSettings.TOOLTIP_TIMEOUT);
    setTimeout(() => this.cantAdd = false, AppSettings.TOOLTIP_TIMEOUT);
  }

  redirect(path) {
    this.router.navigate([path]);
  }

  onSubmit() {
    this.submitted = true;
    this.communucation.type = AppSettings.CATEGORY;
    this.communucation.name = this.categoryForm.get('categoryName').value;

    if (this.categoryForm.invalid === true) {
      return;
    } else {
      this.communicationService.getCommunicationInquiry(AppSettings.CATEGORY).subscribe(data => {
        if (!data.data.some(e => e.name.toUpperCase() === this.communucation.name.toUpperCase())) {
          this.communicationService.addCommunication(this.communucation).subscribe(val => {
            this.communucationResponse = val;
            this.categoryForm.get('categoryName').setValue('');
            this.added = true;
            this.cantAdd = false;
          });
        } else {
          this.added = false;
          this.cantAdd = true;
        }
        this.timeoutTooltip();
        this.submitted = false;
      });
    }
  }
}
