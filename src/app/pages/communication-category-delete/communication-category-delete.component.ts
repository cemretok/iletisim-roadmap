import { Component, OnInit } from '@angular/core';
import { CommunicationService } from 'src/app/services/communication.service';
import { AppSettings } from 'src/app/AppSettings';
import { NgSelectConfig } from '@ng-select/ng-select';

@Component({
  selector: 'app-communication-category-delete',
  templateUrl: './communication-category-delete.component.html',
  styleUrls: ['./communication-category-delete.component.scss']
})
export class CommunicationCategoryDeleteComponent implements OnInit {

  public data = [];
  category = [];
  selectedCategory = [];
  showCantDelete = false;
  showNull = false;



  constructor(private communicationService: CommunicationService,
    private config: NgSelectConfig) {
    this.config.notFoundText = 'Bulunamadı';
  }

  ngOnInit() {
    this.showCantDelete = false;
    this.communicationService.getCommunicationInquiry(AppSettings.CATEGORY).subscribe(val => this.category = val.data);
  }

  timeoutTooltip() {
    setTimeout(() => this.showCantDelete = false, AppSettings.TOOLTIP_TIMEOUT);
    setTimeout(() => this.showNull = true, AppSettings.TOOLTIP_TIMEOUT);
  }

  deleteSelected() {
    this.showCantDelete = false;
    this.showNull = false;

    if (this.selectedCategory.length === 0) {
      this.showNull = true;
    } else {
      this.selectedCategory.forEach((sck, scv) => {
        this.category.forEach((ck, cv) => {
          if (sck === ck.id) {
            this.communicationService.deleteCommunication(ck).subscribe(val => {
              if (val.returnCode === AppSettings.RETURN_CODE_SUCCESS) {
                window.location.reload();
              } else if (val.returnCode === AppSettings.RETURN_CODE_ERROR) {
                this.showCantDelete = true;
                this.selectedCategory = [];
                this.timeoutTooltip();
              }
            });
          }
        });
      });
    }
  }
}
