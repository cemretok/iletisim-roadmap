import { AppSettings } from './../../AppSettings';
import { Component, OnInit } from '@angular/core';
import { CommunicationService } from 'src/app/services/communication.service';

@Component({
    selector: 'app-communication-category',
    templateUrl: './communication-category.component.html',
    styleUrls: ['./communication-category.component.scss']
})
export class CommunicationCategoryComponent implements OnInit {

    public data = [];
    category = [];
    selectedCategory = [];



    constructor(private communicationService: CommunicationService) {
    }

    ngOnInit() {
        this.communicationService.getCommunicationInquiry(AppSettings.CATEGORY).subscribe(val => this.category = val.data);
    }
}
