import { AppSettings } from 'src/app/AppSettings';
import { Component, OnInit, Injectable } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/data/Product';
import { Type } from 'src/app/data/Type';
import { Category } from 'src/app/data/Category';
import { of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ProductResponse } from 'src/app/data/ProductResponse';
import { Router, ActivatedRoute } from '@angular/router';
import { CommunicationService } from 'src/app/services/communication.service';
import { NgSelectConfig } from '@ng-select/ng-select';
import { ProductDataService } from 'src/app/services/product-data.service';
import { TimelineDetail } from 'src/app/data/TimelineDetail';

const I18N_VALUES = {
  tr: {
    weekdays: ['Pa', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct', 'Pz'],
    months: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
  }
};

@Injectable()
export class I18n {
  language = 'tr';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  constructor(private i18n: I18n) {
    super();
  }

  getWeekdayShortName(weekday: number): string {
    return I18N_VALUES[this.i18n.language].weekdays[weekday - 1];
  }
  getMonthShortName(month: number): string {
    return I18N_VALUES[this.i18n.language].months[month - 1];
  }
  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  providers: [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }]
})
export class ProductComponent implements OnInit {

  model: NgbDateStruct;
  public data = [];
  category = [];
  type = [];
  color = [];
  selectedCategory;
  selectedType;
  selectedColor;
  productForm: any;
  isFormInvalid = true;
  submitted = false;
  isButtonAble = true;
  categoryRequired = true;
  typeRequired = true;
  colorRequired = true;
  dateRequired = true;
  beginEndDate;
  productName;
  submitText = AppSettings.SUBMIT_TEXT_ADD;
  productNameLenght = AppSettings.PRODUCT_NAME_LENGHT;
  budgetLenght = AppSettings.BUDGET_LENGHT;
  explanationLenght = AppSettings.EXPLENATION_LENGHT;
  productObject = {} as Product;
  timelineDetailsObject = {} as TimelineDetail;
  typeObject = {} as Type;
  categoryObject = {} as Category;
  productResponse: ProductResponse;
  showOKFileStatus = false;
  showFileLink = false;
  showFileUpload = false;
  base64File;
  base64FileName;
  successText = '';

  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;
  dateLabel: string;

  constructor(private router: Router,
    private calendar: NgbCalendar,
    private productService: ProductService,
    private communicationService: CommunicationService,
    private activatedRoute: ActivatedRoute,
    private config: NgSelectConfig,
    private productDataService: ProductDataService) {
    this.config.notFoundText = 'Bulunamadı';


    this.refreshDateLabel();
  }

  ngOnInit() {
    this.communicationService.getCommunicationInquiry(AppSettings.CATEGORY).subscribe(val => this.category = val.data);
    this.communicationService.getCommunicationInquiry(AppSettings.TYPE).subscribe(val => this.type = val.data);

    this.color = [{
      id: 'black',
      name: 'Siyah',
    }, {
      id: 'red',
      name: 'Kırmızı',
    }, {
      id: 'yellow',
      name: 'Sarı',
    }, {
      id: 'green',
      name: 'Yeşil',
    }, {
      id: 'orange',
      name: 'Turuncu',
    }, {
      id: 'pink',
      name: 'Pembe',
    }, {
      id: 'blue',
      name: 'Mavi',
    }, {
      id: 'purple',
      name: 'Mor',
    }, {
      id: 'gray',
      name: 'Gri',
    }, {
      id: 'brown',
      name: 'Kahverengi',
    }];

    this.productForm = new FormGroup({
      productName: new FormControl(''),
      budget: new FormControl(''),
      explanation: new FormControl(''),
      requestOwner: new FormControl(''),
      beginEndDate: new FormControl('')
    });

    this.activatedRoute.paramMap.subscribe(params => {
      if (params.has('id') && (params.get('mode') === AppSettings.MODE_CLONE)) {
        this.submitText = AppSettings.SUBMIT_TEXT_CLONE;

        this.productService.getProductInquiryById(params.get('id')).subscribe(val => {
          const from = val.data.startDate.split('-', 3);
          const to = val.data.endDate.split('-', 3);

          this.productObject.id = Number(params.get('id'));
          this.productForm.get('productName').setValue(val.data.name);
          this.fromDate = new NgbDate(Number(from[2]), Number(from[1]), Number(from[0]));
          this.toDate = new NgbDate(Number(to[2]), Number(to[1]), Number(to[0]));
          this.productForm.get('budget').setValue(val.data.budget);
          this.productForm.get('explanation').setValue(val.data.description);
          this.selectedColor = val.data.color;
          this.selectedCategory = val.data.category.id;
          this.selectedType = val.data.type.id;
          this.productForm.get('requestOwner').setValue(val.data.requestOwner);
          this.base64File = val.data.file;
          this.base64FileName = val.data.fileName;
          this.showFileLink = val.data.fileName == null ? false : true;
          this.showFileUpload = true;
          this.refreshDateLabel();
        });
      } else if (params.has('id') && (params.get('mode') === AppSettings.MODE_UPDATE)) {
        this.submitText = AppSettings.SUBMIT_TEXT_UPDATE;

        this.productService.getProductInquiryById(params.get('id')).subscribe(val => {
          const from = val.data.startDate.split('-', 3);
          const to = val.data.endDate.split('-', 3);
          this.productObject.id = Number(params.get('id'));
          this.productForm.get('productName').setValue(val.data.name);
          this.fromDate = new NgbDate(Number(from[2]), Number(from[1]), Number(from[0]));
          this.toDate = new NgbDate(Number(to[2]), Number(to[1]), Number(to[0]));
          this.productForm.get('budget').setValue(val.data.budget);
          this.productForm.get('explanation').setValue(val.data.description);
          this.selectedColor = val.data.color;
          this.selectedCategory = val.data.category.id;
          this.selectedType = val.data.type.id;
          this.productForm.get('requestOwner').setValue(val.data.requestOwner);
          this.base64File = val.data.file;
          this.base64FileName = val.data.fileName;
          this.showFileLink = val.data.fileName == null ? false : true;
          this.showFileUpload = true;
          this.refreshDateLabel();
        });
      } else {
        this.showFileLink = false;
        this.showFileUpload = true;
      }
    });
  }

  get f() { return this.productForm.controls; }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      this.toDate = date;
    } else if ((this.fromDate && this.toDate) && (this.fromDate.equals(this.toDate)) && date.after(this.fromDate)) {
      this.toDate = date;
    } else if ((this.fromDate && this.toDate) && (!this.fromDate.equals(this.toDate))) {
      this.fromDate = date;
      this.toDate = null;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.fromDate = null;
      this.toDate = null;
    }

    this.refreshDateLabel();
  }

  refreshDateLabel() {
    this.dateLabel = this.getDayAriaLabel(this.fromDate, this.toDate);
  }

  getDayAriaLabel(from: NgbDateStruct, to: NgbDateStruct): string {
    if (this.fromDate && this.toDate) {
      return `${from.day}/${from.month}/${from.year} - ${to.day}/${to.month}/${to.year}`;
    } else if (this.fromDate && !this.toDate) {
      return `${from.day}/${from.month}/${from.year} - `;
    } else if (!this.fromDate && !this.toDate) {
      return ``;
    }

    return ``;
  }

  getFromDateServerFormat(): string {
    return `${this.fromDate.day}-${this.fromDate.month}-${this.fromDate.year}`;
  }

  getToDateServerFormat(): string {
    return `${this.toDate.day}-${this.toDate.month}-${this.toDate.year}`;
  }

  setNextWeek(week: number) {
    if (this.fromDate) {
      this.toDate = this.calendar.getNext(this.fromDate, 'd', 7 * week);
    }

    this.refreshDateLabel();
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  fileChange(event) {
    this.showOKFileStatus = false;
    this.showFileLink = false;
    this.showFileUpload = true;
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.base64File = reader.result;
        this.base64FileName = file.name;
        this.showOKFileStatus = true;
      };
    }
  }

  makeFile() {
    const linkSource = this.base64File;
    const downloadLink = document.createElement('a');
    const fileName = this.base64FileName;

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  redirectToIndex() {
    setTimeout(() => { this.router.navigate(['index']); }, 2000);
  }

  onSubmit() {
    this.submitted = true;

    if (this.productForm.invalid === true) {
      this.isButtonAble = true;
      return;
    } else {
      this.isButtonAble = false;
      this.productObject.name = this.productForm.value.productName;
      this.productObject.startDate = this.getFromDateServerFormat();
      this.productObject.endDate = this.getToDateServerFormat();
      this.productObject.budget = this.productForm.value.budget;
      this.productObject.description = this.productForm.value.explanation;
      this.productObject.color = this.selectedColor;
      this.categoryObject.id = this.selectedCategory;
      this.typeObject.id = this.selectedType;
      this.productObject.category = this.categoryObject;
      this.productObject.type = this.typeObject;
      this.productObject.requestOwner = this.productForm.value.requestOwner;
      this.productObject.file = this.base64File;
      this.productObject.fileName = this.base64FileName;


      if (this.submitText === AppSettings.SUBMIT_TEXT_ADD) {
        this.productService.addProduct(this.productObject).pipe(
          retry(2),
          catchError(() => {
            return of({});
          })).subscribe(object => {
            this.productResponse = object as ProductResponse;
            if (this.productResponse.returnCode === AppSettings.RETURN_CODE_SUCCESS) {
              if (this.submitText === AppSettings.SUBMIT_TEXT_ADD) {
                this.successText = AppSettings.ADDING_TEXT;
              }
              this.isButtonAble = false;
              this.redirectToIndex();
            }
          });
      } else if (this.submitText === AppSettings.SUBMIT_TEXT_CLONE) {
          this.timelineDetailsObject.id = Number(sessionStorage.getItem('timeline'));
          this.productObject.id = null;
          this.timelineDetailsObject.product = this.productObject;
          this.productService.addToTimelineProduct(this.timelineDetailsObject).pipe(
            retry(2),
            catchError(() => {
              return of({});
            })).subscribe(object => {
              this.productResponse = object as ProductResponse;
              if (this.productResponse.returnCode === AppSettings.RETURN_CODE_SUCCESS) {
                if (this.submitText === AppSettings.SUBMIT_TEXT_CLONE) {
                  this.successText = AppSettings.CLONE_TEXT;
                }

                this.isButtonAble = false;
                this.redirectToIndex();
              }
            });
      } else if (this.submitText === AppSettings.SUBMIT_TEXT_UPDATE) {
        this.productService.updateProduct(this.productObject).pipe(
          retry(2),
          catchError(() => {
            return of({});
          })).subscribe(object => {
            this.productResponse = object as ProductResponse;
            if (this.productResponse.returnCode === AppSettings.RETURN_CODE_SUCCESS) {
              if (this.submitText === AppSettings.SUBMIT_TEXT_UPDATE) {
                this.successText = AppSettings.UPDATE_TEXT;
              }

              this.isButtonAble = false;
              this.redirectToIndex();
            }
          });
      }
    }
  }
}
