import { Component, OnInit } from '@angular/core';
import { CommunicationService } from 'src/app/services/communication.service';
import { AppSettings } from 'src/app/AppSettings';
import { NgSelectConfig } from '@ng-select/ng-select';

@Component({
  selector: 'app-communication-type-delete',
  templateUrl: './communication-type-delete.component.html',
  styleUrls: ['./communication-type-delete.component.scss']
})
export class CommunicationTypeDeleteComponent implements OnInit {

  public data = [];
  type = [];
  selectedType = [];
  showCantDelete = false;
  showNull = false;



  constructor(private communicationService: CommunicationService,
    private config: NgSelectConfig) {
    this.config.notFoundText = 'Bulunamadı';
  }

  ngOnInit() {
    this.showCantDelete = false;
    this.communicationService.getCommunicationInquiry(AppSettings.TYPE).subscribe(val => this.type = val.data);
  }

  timeoutTooltip() {
    setTimeout(() => this.showCantDelete = false, AppSettings.TOOLTIP_TIMEOUT);
    setTimeout(() => this.showNull = false, AppSettings.TOOLTIP_TIMEOUT);
  }

  deleteSelected() {
    this.showCantDelete = false;
    this.showNull = false;

    if (this.selectedType.length === 0) {
      this.showNull = true;
    } else {
      this.selectedType.forEach((sck, scv) => {
        this.type.forEach((ck, cv) => {
          if (sck === ck.id) {
            this.communicationService.deleteCommunication(ck).subscribe(val => {
              if (val.returnCode === AppSettings.RETURN_CODE_SUCCESS) {
                window.location.reload();
              } else if (val.returnCode === AppSettings.RETURN_CODE_ERROR) {
                this.showCantDelete = true;
                this.selectedType = [];
                this.timeoutTooltip();
              }
            });
          }
        });
      });
    }
  }
}
