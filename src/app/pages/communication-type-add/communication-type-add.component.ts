
import { AppSettings } from '../../AppSettings';
import { Component, OnInit } from '@angular/core';
import { CommunicationService } from 'src/app/services/communication.service';
import { Communication } from 'src/app/data/Communication';
import { CommunicationResponse } from 'src/app/data/CommunicationResponse';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-communication-type-add',
  templateUrl: './communication-type-add.component.html',
  styleUrls: ['./communication-type-add.component.scss']
})
export class CommunicationTypeAddComponent implements OnInit {

  communucation = {} as Communication;
  communucationResponse = {} as CommunicationResponse;
  typeName;
  added = false;
  cantAdd = false;
  typeNameLenght = AppSettings.TYPE_NAME_LENGHT;
  typeForm: any;
  submitted = false;

  constructor(private communicationService: CommunicationService,
              private router: Router) {
  }

  ngOnInit() {
    this.typeForm = new FormGroup({
      typeName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(this.typeNameLenght)])
    });
  }

  get f() { return this.typeForm.controls; }

  formChange() {
    this.submitted = false;
    this.added = false;
    this.cantAdd = false;
  }

  redirect(path) {
    this.router.navigate([path]);
  }

  timeoutTooltip() {
    setTimeout(() => this.added = false, AppSettings.TOOLTIP_TIMEOUT);
  }

  onSubmit() {
    this.submitted = true;
    this.communucation.type = AppSettings.TYPE;
    this.communucation.name = this.typeForm.get('typeName').value;

    if (this.typeForm.invalid === true) {
      return;
    } else {
      this.communicationService.getCommunicationInquiry(AppSettings.TYPE).subscribe(data => {
        if (!data.data.some(e => e.name.toUpperCase() === this.communucation.name.toUpperCase())) {
          this.communicationService.addCommunication(this.communucation).subscribe(val => {
            this.communucationResponse = val;
            this.typeName = null;
            this.typeForm.get('typeName').setValue('');
            this.added = true;
            this.cantAdd = false;
          });
        } else {
          this.added = false;
          this.cantAdd = true;
        }
        this.timeoutTooltip();
        this.submitted = false;
      });
    }
  }
}
