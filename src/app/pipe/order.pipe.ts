import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "order"
})
export class OrderPipe implements PipeTransform {
    transform(array: any, field: string): any[] {
        var orderType = 'ASC';
        if (!Array.isArray(array)) {
            return;
        }

        if (field[0] === '-') {
            field = field.substring(1);
            orderType = 'DESC';
        }
        array.sort((a: any, b: any) => {
            if (orderType === 'ASC') {
                if (a[field] < b[field]) return -1;
                if (a[field] > b[field]) return 1;
                return 0;
            } else {
                if (a[field] < b[field]) return 1;
                if (a[field] > b[field]) return -1;
                return 0;
            }
        });
        return array;
    }
}
