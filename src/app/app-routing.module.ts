import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { CommunicationTypeComponent } from './pages/communication-type/communication-type.component';
import { CommunicationCategoryComponent } from './pages/communication-category/communication-category.component';
import { ProductComponent } from './pages/product/product.component';
import { CommunicationTypeAddComponent } from './pages/communication-type-add/communication-type-add.component';
import { CommunicationCategoryAddComponent } from './pages/communication-category-add/communication-category-add.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { CommunicationTypeDeleteComponent } from './pages/communication-type-delete/communication-type-delete.component';
import { CommunicationCategoryDeleteComponent } from './pages/communication-category-delete/communication-category-delete.component';

const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: IndexComponent, canActivate: [AuthGuardService] },
  { path: 'iletisim-tipi', component: CommunicationTypeComponent, canActivate: [AuthGuardService] },
  { path: 'iletisim-tipi-ekle', component: CommunicationTypeAddComponent, canActivate: [AuthGuardService] },
  { path: 'iletisim-tipi-sil', component: CommunicationTypeDeleteComponent, canActivate: [AuthGuardService] },
  { path: 'iletisim-kategori', component: CommunicationCategoryComponent, canActivate: [AuthGuardService] },
  { path: 'iletisim-kategori-ekle', component: CommunicationCategoryAddComponent, canActivate: [AuthGuardService] },
  { path: 'iletisim-kategori-sil', component: CommunicationCategoryDeleteComponent, canActivate: [AuthGuardService] },
  { path: 'urun', component: ProductComponent, canActivate: [AuthGuardService] },
  { path: 'urun/:mode/:id', component: ProductComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [IndexComponent, CommunicationTypeComponent, CommunicationCategoryComponent, ProductComponent];
