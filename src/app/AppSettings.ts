export class AppSettings {
    public static CATEGORY = 'CATEGORY';
    public static TYPE = 'TYPE';
    public static PRODUCT_NAME_LENGHT = 500;
    public static TYPE_NAME_LENGHT = 150;
    public static CATEGORY_NAME_LENGHT = 150;
    public static BUDGET_LENGHT = 999999999;
    public static EXPLENATION_LENGHT = 1000;
    public static RETURN_CODE_SUCCESS = "0";
    public static RETURN_CODE_ERROR = "1";
    public static MODE_CLONE = "clone";
    public static MODE_UPDATE = "update";
    public static SUBMIT_TEXT_ADD = "Ekle";
    public static SUBMIT_TEXT_CLONE = "Kopyala";
    public static SUBMIT_TEXT_UPDATE = "Güncelle";
    public static UPDATE_TEXT = 'güncelleme';
    public static ADDING_TEXT = 'ekleme';
    public static CLONE_TEXT = 'kopyalama';
    public static TOOLTIP_TIMEOUT = 3000;
}
