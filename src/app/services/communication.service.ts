import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CommunicationResponse } from '../data/CommunicationResponse';
import { Communication } from '../data/Communication';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  constructor(private http: HttpClient) { }

  getCommunicationInquiry(type: string): Observable<CommunicationResponse> {
    const params = new HttpParams().set('type', type);
    return this.http.get<CommunicationResponse>(environment.getCommunicationInquiry, { params: params });
  }

  addCommunication(communucation: Communication): Observable<CommunicationResponse> {
    return this.http.post<CommunicationResponse>(environment.addCommunication, communucation);
  }

  updateCommunication(communucation: Communication): Observable<CommunicationResponse> {
    return this.http.post<CommunicationResponse>(environment.updateCommunication, communucation);
  }

  deleteCommunication(communucation: Communication): Observable<CommunicationResponse> {
    return this.http.post<CommunicationResponse>(environment.deleteCommunication, communucation);
  }
}
