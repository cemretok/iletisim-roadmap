import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Timeline } from '../data/Timeline';
import { environment } from 'src/environments/environment';
import { TimelineData } from '../data/TimelineData';

@Injectable({
  providedIn: 'root'
})
export class TimelineService {

  constructor(private http: HttpClient) { }

  getTimelineInquiry(): Observable<Timeline> {
    return this.http.get<Timeline>(environment.getTimelineInquiry);
  }

  getTimelineInquiryById(id: string): Observable<Timeline> {
    const params = new HttpParams().set('id', id);
    return this.http.get<Timeline>(environment.getTimelineInquiryById, { params: params });
  }

  getTimelineInquiryLastOne(): Observable<Timeline> {
    return this.http.get<Timeline>(environment.getTimelineInquiryLastOne);
  }

  getTimelineInquiryActiveList(): Observable<Timeline> {
    return this.http.get<Timeline>(environment.getTimelineInquiryActiveList);
  }

  addTimeline(timelineData: any): Observable<Timeline> {
    return this.http.post<Timeline>(environment.addTimeline, timelineData);
  }

  createExcel(timelineId: any) {
    return environment.excel + '?timelineId=' + timelineId;
  }
}
