import { AppSettings } from './../AppSettings';
import { CommunicationResponse } from './../data/CommunicationResponse';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AuthResponse } from '../data/AuthResponse';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private http: HttpClient, private router: Router) { }

  authenticate(username, password) {
    return this.basitAuth(username, password).pipe(
      retry(2),
      catchError(() => {
        return of({});
      }));
  }

  isUserLoggedIn() {
    const user = sessionStorage.getItem('username');
    return !(user === null);
  }

  logOut() {
    sessionStorage.removeItem('username');
  }

  basitAuth(username, password): Observable<AuthResponse> {
    return this.http.post<any>(environment.basicAuth,
      { 'userName': username, 'pass': password });
  }

  getUser() {
    return sessionStorage.getItem('username');
  }
}
