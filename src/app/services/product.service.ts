import { Communication } from './../data/Communication';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductResponse } from '../data/ProductResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../data/Product';
import { ProductResponseOne } from '../data/ProductResponseOne';
import { TimelineDetail } from '../data/TimelineDetail';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProductInquiry(): Observable<ProductResponse> {
    return this.http.get<ProductResponse>(environment.getProductInquiry);
  }

  getProductInquiryById(id: string): Observable<ProductResponseOne> {
    return this.http.get<ProductResponseOne>(environment.getProductInquiryById + '/' + id);
  }

  inquiryByCategoryAndType(communication: Communication[]): Observable<ProductResponse> {
    return this.http.post<ProductResponse>(environment.getProductInquiryByCategoryAndType, communication);
  }

  addProduct(product: Product): Observable<ProductResponse> {
    return this.http.post<ProductResponse>(environment.addProduct, product);
  }

  addToTimelineProduct(timelineDetail: TimelineDetail): Observable<ProductResponse> {
    return this.http.post<ProductResponse>(environment.addToTimelineProduct, timelineDetail);
  }

  updateProduct(product: Product): Observable<ProductResponse> {
    return this.http.post<ProductResponse>(environment.updateProduct, product);
  }

  deleteProduct(product: Product): Observable<ProductResponse> {
    return this.http.post<ProductResponse>(environment.deleteProduct, product);
  }
}
