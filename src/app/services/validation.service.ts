import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  static nameValidator(control) {
    if (control.value.match(/[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)) {
      return null;
    } else {
      return { 'invalidName': true };
    }
  }
}