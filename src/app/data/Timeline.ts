import { TimelineData } from './TimelineData';

export interface Timeline {
    returnCode: string;
    returnMsg: string;
    data: TimelineData;
}
