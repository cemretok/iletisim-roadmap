import { TimelineDetails } from './TimelineDetails';

export interface TimelineData {
    id: number;
    name: string;
    createDate: string;
    timelineDetails: TimelineDetails[];
}
