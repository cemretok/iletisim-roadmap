import { Product } from './Product';

export interface TimelineDetail {
    id: number;
    product: Product;
}
