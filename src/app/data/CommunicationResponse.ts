import { Communication } from './Communication';

export interface CommunicationResponse {
    returnCode: string;
    returnMsg: string;
    data: Communication[];
}
