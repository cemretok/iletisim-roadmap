export interface Type {
    id: number;
    name: string;
    type: string;
}
