import { Category } from './Category';
import { Type } from './Type';

export interface Product {
    id: number;
    name: string;
    startDate: string;
    endDate: string;
    budget: number;
    description: string;
    color: string;
    file: string;
    fileName: string;
    category: Category;
    type: Type;
    requestOwner: string;
}
