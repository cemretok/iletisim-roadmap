import { Product } from './Product';

export interface ProductResponse {
    returnCode: string;
    returnMsg: string;
    data: Product[];
}
