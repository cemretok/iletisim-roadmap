import { Product } from './Product';

export interface ProductResponseOne {
    returnCode: string;
    returnMsg: string;
    data: Product;
}
