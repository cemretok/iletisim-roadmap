import { Product } from './Product';

export interface TimelineDetails {
    id: number;
    product: Product[];
}
