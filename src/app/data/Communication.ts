export interface Communication {
    id: number;
    name: string;
    type: string;
}
