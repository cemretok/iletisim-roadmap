export interface AuthResponse {
    returnCode: string;
    returnMsg: string;
    data: any;
}
